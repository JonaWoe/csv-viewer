package util.extensions

import models.Table
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class TableExtensionsTest {

    @Test
    fun `build() - table object - pretty table`() {
        val table = Table("A","B", listOf("C", "D"))
        val expected = listOf("A","B", "C","D")

        val actual = table.build()

        assertEquals(expected, actual)
    }
}
