package models

class TablePage (
    var table: Table,
    var currentPage: Int,
    var maxPage: Int
)
