package interactors

import models.Table
import models.TablePage
import services.CommandLineService
import services.CsvService
import services.PageService
import services.TableService
import util.extensions.copy
import view.ConsoleViewService

class TableInteractor(
    commandLineService: CommandLineService,
    consoleViewService: ConsoleViewService,
    tableService: TableService,
    csvService: CsvService,
    private val pageService: PageService,
) {
    private var pageSize: Int
    private var fullTable: Table

    init {

        consoleViewService.setCallbacks(
            { goToFirstPage() },
            { goToLastPage() },
            { goToNextPage() },
            { goToPreviousPage() },
            { pageCounter:Int -> goToPage(pageCounter) }
        )

        val csvFileName = commandLineService.parseCsvFileName()
        val rawLines = csvService.readCsvFile(csvFileName)
        fullTable = tableService.formatTable(rawLines)
        pageSize = commandLineService.parsePageSize()
        pageService.calculateLastPageCounter(fullTable.content.size, pageSize)
    }

    fun goToFirstPage(): TablePage {
        pageService.setFirstPageCounter()
        return filterPageContent()
    }

    fun goToLastPage(): TablePage {
        pageService.setLastPageCounter()
        return filterPageContent()
    }

    fun goToPreviousPage(): TablePage {
        pageService.setPreviousPageCounter()
        return filterPageContent()
    }

    fun goToNextPage(): TablePage {
        pageService.setNextPageCounter()
        return filterPageContent()
    }

    fun goToPage(pageCounter: Int): TablePage {
        pageService.setPageCounter(pageCounter)
        return filterPageContent()
    }

    private fun filterPageContent(): TablePage {
        return pageService.filterPageContent(fullTable, pageSize)
    }
}
