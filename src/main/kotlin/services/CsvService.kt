package services

import java.io.File
import java.nio.charset.Charset

class CsvService {

    fun readCsvFile(csvFileName: String): List<String> {
        val csvReader = File(csvFileName).bufferedReader(Charset.forName("UTF-8"))

        val csvLines = mutableListOf<String>()

        csvReader.use {
            while (it.ready()) {
                csvLines.add(it.readLine())
            }
        }
        return csvLines
    }
}
