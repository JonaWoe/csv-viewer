package services

import models.Table
import models.TablePage
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals


class PageServiceTest {

    private lateinit var pageService: PageService

    @BeforeEach
    fun setUp() {
        pageService = PageService()
    }

    @Test
    fun `filterPageContent() - list of 3 elements, page size 2, after init - first page of content `() {
        val table = Table("H","S", listOf("A","B","C"))
        pageService.calculateLastPageCounter(3, 2)

        val actual = pageService.filterPageContent(table, 2)

        val expected = TablePage(Table("H","S", listOf("A","B")), 0,1)
        assertEquals(expected.table, actual.table)
        assertEquals(expected.currentPage, actual.currentPage)
        assertEquals(expected.maxPage, actual.maxPage)
    }

    @Test
    fun `setLastPageCounter() - list of 3 elements, page size 2, go to last - last page of content `() {
        val table = Table("H","S", listOf("A","B","C"))
        pageService.calculateLastPageCounter(3, 2)

        pageService.setLastPageCounter()
        val actual = pageService.filterPageContent(table, 2)

        val expected = TablePage(Table("H","S", listOf("C")), 1,1)
        assertEquals(expected.table, actual.table)
        assertEquals(expected.currentPage, actual.currentPage)
        assertEquals(expected.maxPage, actual.maxPage)
    }

    @Test
    fun `setFirstPageCounter() - list of 3 elements, page size 2, go to last then go to first - first page of content `() {
        val table = Table("H","S", listOf("A","B","C"))
        pageService.calculateLastPageCounter(3, 2)

        pageService.setLastPageCounter()
        pageService.setFirstPageCounter()
        val actual = pageService.filterPageContent(table, 2)

        val expected = TablePage(Table("H","S", listOf("A","B")), 0,1)
        assertEquals(expected.table, actual.table)
        assertEquals(expected.currentPage, actual.currentPage)
        assertEquals(expected.maxPage, actual.maxPage)
    }

    @Test
    fun `setNextPageCounter() - list of 3 elements, page size 2, go to next - next page of content `() {
        val table = Table("H","S", listOf("A","B","C"))
        pageService.calculateLastPageCounter(3, 2)

        pageService.setNextPageCounter()
        val actual = pageService.filterPageContent(table, 2)

        val expected = TablePage(Table("H","S", listOf("C")), 1,1)
        assertEquals(expected.table, actual.table)
        assertEquals(expected.currentPage, actual.currentPage)
        assertEquals(expected.maxPage, actual.maxPage)
    }

    @Test
    fun `setPreviousPageCounter() - list of 3 elements, page size 2, go to last then go to previous - first page of content `() {
        val table = Table("H","S", listOf("A","B","C"))
        pageService.calculateLastPageCounter(3, 2)

        pageService.setPreviousPageCounter()
        pageService.setFirstPageCounter()
        val actual = pageService.filterPageContent(table, 2)

        val expected = TablePage(Table("H","S", listOf("A","B")), 0,1)
        assertEquals(expected.table, actual.table)
        assertEquals(expected.currentPage, actual.currentPage)
        assertEquals(expected.maxPage, actual.maxPage)
    }

    @Test
    fun `setPageCounter() - list of 3 elements, page size 2, go to page 1 - page 1 of content `() {
        val table = Table("H","S", listOf("A","B","C"))
        pageService.calculateLastPageCounter(3, 2)

        pageService.setPageCounter(1)
        val actual = pageService.filterPageContent(table, 2)

        val expected = TablePage(Table("H","S", listOf("C")), 1,1)
        assertEquals(expected.table, actual.table)
        assertEquals(expected.currentPage, actual.currentPage)
        assertEquals(expected.maxPage, actual.maxPage)
    }
}
