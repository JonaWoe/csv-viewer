package util.exceptions

class ArgsNotSpecifiedException(msg: String) : RuntimeException(msg)
