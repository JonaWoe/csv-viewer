package services

import models.Table
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import util.extensions.build
import kotlin.test.assertEquals

class TableServiceTest {

    private lateinit var tableService: TableService

    @BeforeEach
    fun setUp() {
        tableService = TableService()
    }

    @Test
    fun `formatTable() - no missing rows or lines - pretty table`() {
        val inputLines = listOf(
            "Name;Strasse;Ort;Alter",
            "Peter Pan;Am Hang 5;12345 Einsam;42",
            "Maria Schmitz;Kölner Straße 45;50123 Köln;43",
            "Paul Meier;Münchener Weg 1;87654 München;65"
        )

        val actualLines = tableService.formatTable(inputLines).build()

        val expectedLines = listOf(
            "No.|Name         |Strasse         |Ort          |Alter|",
            "---+-------------+----------------+-------------+-----+",
            "1. |Peter Pan    |Am Hang 5       |12345 Einsam |42   |",
            "2. |Maria Schmitz|Kölner Straße 45|50123 Köln   |43   |",
            "3. |Paul Meier   |Münchener Weg 1 |87654 München|65   |"
        )
        assertEquals(expectedLines, actualLines)
    }

    @Test
    fun `formatTable() - missing name element - pretty table`() {
        val inputLines = listOf(
            "Name;Strasse;Ort;Alter",
            ";Am Hang 5;12345 Einsam;42",
            "Maria Schmitz;Kölner Straße 45;50123 Köln;43",
            "Paul Meier;Münchener Weg 1;87654 München;65"
        )

        val actualLines = tableService.formatTable(inputLines).build()

        val expectedLines = listOf(
            "No.|Name         |Strasse         |Ort          |Alter|",
            "---+-------------+----------------+-------------+-----+",
            "1. |             |Am Hang 5       |12345 Einsam |42   |",
            "2. |Maria Schmitz|Kölner Straße 45|50123 Köln   |43   |",
            "3. |Paul Meier   |Münchener Weg 1 |87654 München|65   |"
        )
        assertEquals(expectedLines, actualLines)
    }

    @Test
    fun `formatTable() - missing header element - pretty table`() {
        val inputLines = listOf(
            "Name;Strasse;;Alter",
            "Peter Pan;Am Hang 5;12345 Einsam;42",
            "Maria Schmitz;Kölner Straße 45;50123 Köln;43",
            "Paul Meier;Münchener Weg 1;87654 München;65"
        )

        val actualLines = tableService.formatTable(inputLines).build()

        val expectedLines = listOf(
            "No.|Name         |Strasse         |             |Alter|",
            "---+-------------+----------------+-------------+-----+",
            "1. |Peter Pan    |Am Hang 5       |12345 Einsam |42   |",
            "2. |Maria Schmitz|Kölner Straße 45|50123 Köln   |43   |",
            "3. |Paul Meier   |Münchener Weg 1 |87654 München|65   |"
        )
        assertEquals(expectedLines, actualLines)
    }

    @Test
    fun `addIndexToRows() - one line with 5 columns - 5 Columns`() {
        val inputLines = listOf(
            mutableListOf("A","B","C"),
            mutableListOf("E","F", "G")
        )

        val actualRowCount = tableService.addIndexToRows(inputLines)

        val expectedLines = listOf(
            mutableListOf("No.","A","B","C"),
            mutableListOf("1.","E","F", "G")
        )
        assertEquals(expectedLines, actualRowCount)
    }

    @Test
    fun `countColumns() - all lines with 4 columns - 4 columns`() {
        val inputLines = listOf(
            listOf("Name","Strasse","Ort","Alter"),
            listOf("Peter Pan","Am Hang 5","12345 Einsam","42"),
            listOf("Maria Schmitz","Kölner Straße 45","50123 Köln","43"),
            listOf("Paul Meier","Münchener Weg 1","87654 München","65")
        )

        val actualRowCount = tableService.countColumns(inputLines)

        assertEquals(4, actualRowCount)
    }

    @Test
    fun `countColumns() - one line with 5 columns - 5 Columns`() {
        val inputLines = listOf(
            listOf("Name","Strasse","Ort","Alter"),
            listOf("Peter Pan","Am Hang 5","12345 Einsam","42","peter.pan@gmail.com"),
            listOf("Maria Schmitz","Kölner Straße 45","50123 Köln","43"),
            listOf("Paul Meier","Münchener Weg 1","87654 München","65")
        )

        val actualRowCount = tableService.countColumns(inputLines)

        assertEquals(5, actualRowCount)
    }

    @Test
    fun `buildTable() - pre formated lines - final table`() {
        val inputLines = listOf("A","B", "C")
        val inputHeaderLine = "D"

        val actual = tableService.buildTable(inputLines, inputHeaderLine)

        val expected = Table("A", "D", listOf("B", "C"))
        assertEquals(expected, actual)
    }

    @Test
    fun `splitRowInColumns() - header - list of header rows`() {
        val inputLine = "Name;Strasse;Ort;Alter"
        val expectedRows = listOf("Name","Strasse","Ort","Alter")

        val actualRows = tableService.splitRowInColumns(inputLine)

        assertEquals(expectedRows, actualRows)
    }

    @Test
    fun `formatRow() - header - pretty header`() {
        val inputRows = listOf("Name","Strasse","Ort","Alter")
        val inputRowsLength = listOf(7,7,7,7)

        val actualRows = tableService.formatRow(inputRows, inputRowsLength)

        val expectedStrings = "Name   |Strasse|Ort    |Alter  |"
        assertEquals(expectedStrings, actualRows)
    }

    @Test
    fun `generateHeaderLine() - line length of 5,7,5,5 - correct header line`() {
        val inputRowsLength = listOf(5,7,5,5)

        val actualLine = tableService.generateHeaderLine(4, inputRowsLength)

        val expectedLine = "-----+-------+-----+-----+"
        assertEquals(expectedLine, actualLine)
    }

    @Test
    fun `calculateMaxColumnSize() - only one row with 5 columns - 5`() {
        val inputLines= listOf(
            listOf("Name","Strasse","Ort","Alter"),
            listOf("Peter Pan","Am Hang 5","12345 Einsam","42"),
            listOf("Maria Schmitz","Kölner Straße 45","50123 Köln","43"),
            listOf("Paul Meier","Münchener Weg 1","87654 München","65")
        )

        val actualString = tableService.calculateMaxColumnSize(inputLines, 4)

        val expectedRowsLength = listOf(13, 16, 13, 5)
        assertEquals(expectedRowsLength, actualString)
    }

    @Test
    fun `generateNumberOfChars() - 5 times Minus - -----`() {
        val actualString = tableService.generateNumberOfChars(5, '-')

        assertEquals("-----", actualString)
    }
}
