package view

import models.TablePage
import util.extensions.build

class ConsoleViewService {

    companion object {
        const val  helpText = "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit"
    }

    init {
        println("INFO: CSV-Viewer started.")
    }

    lateinit var goToFirstPageCallback: () -> TablePage
    lateinit var goToLastPageCallback: () -> TablePage
    lateinit var goToNextPageCallback: () -> TablePage
    lateinit var goToPreviousPageCallback: () -> TablePage
    lateinit var goToPageCallback: (pageCounter: Int) -> TablePage

    fun setCallbacks(
        goToFirstPageCallback: () -> TablePage,
        goToLastPageCallback: () -> TablePage,
        goToNextPageCallback: () -> TablePage,
        goToPreviousPageCallback: () -> TablePage,
        goToPageCallback:  (pageCounter: Int) -> TablePage,
    ) {
        this.goToFirstPageCallback = goToFirstPageCallback
        this.goToLastPageCallback = goToLastPageCallback
        this.goToNextPageCallback = goToNextPageCallback
        this.goToPreviousPageCallback = goToPreviousPageCallback
        this.goToPageCallback = goToPageCallback
    }

    var exit = false
    var jumpSelected = false

    fun start() {
        executeAndRefreshView { goToFirstPageCallback() }

        while (!exit) {

            try {
                handleInput(readln())
            } catch (exception: Exception) {
                println("ERROR: Could not read console input exiting. Exiting CSV-Viewer!")
                return
            }
        }

        println("INFO: CSV-Viewer stopped.")
    }

    fun handleInput(input: String) {
        val pattern = Regex("^[+]?\\d*\$")

        when {
            input == "F" && !jumpSelected -> {
                executeAndRefreshView { goToFirstPageCallback() }
            }
            input == "L" && !jumpSelected -> {
                executeAndRefreshView { goToLastPageCallback() }
            }
            input == "P" && !jumpSelected -> {
                executeAndRefreshView { goToPreviousPageCallback() }
            }
            input == "N" && !jumpSelected -> {
                executeAndRefreshView { goToNextPageCallback() }
            }
            input == "J" && !jumpSelected -> {
                selectJump()
            }
            input.matches(pattern) && jumpSelected -> {
                val pageCounter = Integer.parseInt(input)-1
                executeAndRefreshView { goToPageCallback(pageCounter)}
            }
            input == "E" -> exit = true
            else -> printCommandNotSupported()
        }
    }

    fun selectJump() {
        jumpSelected = true
        println("INFO: Jump to page selected, insert page number:")
    }

    fun executeAndRefreshView(function: () -> TablePage) {
        jumpSelected=false
        val currentPage = function.invoke()
        refreshConsoleView(currentPage)
    }

    fun refreshConsoleView(currentPage: TablePage) {
        currentPage.table.build().map { println(it) }
        println("Page ${currentPage.currentPage+1} of ${currentPage.maxPage+1}")
        println(helpText)
    }

    fun printCommandNotSupported() {
        println("Command not supported: $helpText")
    }

    fun printPageSize(pageSize: Int) {
        println("INFO: Using page size of $pageSize.")
    }

    fun printDefaultPageSize(defaultPageSize: Int) {
        println("WARNING: Could not parse page size! Using default page size of $defaultPageSize.")
    }

    fun printExitingCsvViewer() {
        println("ERROR: No arguments specified. Exiting CSV-Viewer!")
    }
}
