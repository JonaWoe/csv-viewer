package services

import models.Table

class TableService {


    fun formatTable(inputStrings: List<String>): Table {
        val splitRows = inputStrings.map { splitRowInColumns(it) }
        val indexedRows = addIndexToRows(splitRows)
        val columnCount = countColumns(indexedRows)
        val maxColumnSize = calculateMaxColumnSize(indexedRows, columnCount )
        val formatRow = splitRows.map { formatRow(it, maxColumnSize) }
        val headerLine = generateHeaderLine(columnCount, maxColumnSize)
        return buildTable(formatRow, headerLine)
    }

    fun buildTable(formatRow:List<String>, separator: String): Table {
        val header = formatRow.first()
        val content = formatRow.filterIndexed { index, _ -> index != 0}

        return Table(header, separator, content)
    }

    fun splitRowInColumns(row: String): List<String> {
        return row.split(";")
    }

    fun countColumns(rowsAndColumns:List<List<String>>): Int {
        return rowsAndColumns.map { it.count() }.maxOf { it }
    }

    fun addIndexToRows(rowsAndColumns:List<List<String>>): List<List<String>> {

        val enrichedRowsAndColumns = mutableListOf<List<String>>()

        rowsAndColumns.forEachIndexed { index, row ->
            val newElement = if (index == 0) {
                "No."
            } else {
                "$index."
            }
            (row as MutableList<String>).add(0, newElement)
            enrichedRowsAndColumns.add(row)
        }

        return  enrichedRowsAndColumns
    }

    fun calculateMaxColumnSize(row: List<List<String>>, columnCount: Int): List<Int> {

        val result = mutableListOf<Int>()

        for (item: Int in 0 until  columnCount)
            row.forEachIndexed { _, element ->
                element.forEachIndexed { index, entry ->
                    if (index == item) {
                        if (result.size<=item) {
                            result.add(entry.length)
                        } else if (result[item] < entry.length) {
                            result[item] = entry.length
                        }
                    }
                }
            }
        return result
    }

    fun formatRow(row: List<String>, maxColumnCount: List<Int>): String {
        var mergedRow = ""

        row.forEachIndexed { index, it ->
            val spaceCount = maxColumnCount[index]-it.length
            val rowWithSpaces = "$it${generateNumberOfChars(spaceCount, ' ')}"
            mergedRow = "$mergedRow$rowWithSpaces|" }
        return mergedRow
    }

    fun generateHeaderLine(columnCount: Int, maxColumnRow: List<Int>): String {
        var headerLine = ""
        for (columnCounter: Int in 0 until columnCount) {
            val line = generateNumberOfChars(maxColumnRow[columnCounter], '-')
            headerLine = "$headerLine$line+"
        }
        return headerLine
    }

    fun generateNumberOfChars(numberOfChars: Int, char: Char): String {
        var result = ""
        for (index: Int in 0 until numberOfChars) {
            result = "$result$char"
        }
        return result
    }
}
