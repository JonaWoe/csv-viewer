package view

import com.nhaarman.mockitokotlin2.*
import models.Table
import models.TablePage
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ConsoleViewServiceTest {

    private lateinit var consoleViewService: ConsoleViewService

    private val standardOut = System.out
    private val outputStreamCaptor = ByteArrayOutputStream()

    @Mock
    lateinit var goToFirstPageCallback: () -> TablePage

    @Mock
    lateinit var goToLastPageCallback: () -> TablePage

    @Mock
    lateinit var goToNextPageCallback: () -> TablePage

    @Mock
    lateinit var goToPreviousPageCallback: () -> TablePage

    @Mock
    lateinit var goToPageCallback: (pageCounter: Int) -> TablePage

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        consoleViewService = ConsoleViewService()

        System.setOut(PrintStream(outputStreamCaptor));
    }

    @Test
    fun `init{} - print csv viewer started - csv viewer started as console output`() {

        ConsoleViewService()

        val expected = "INFO: CSV-Viewer started."
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }



    @Test
    fun `setCallbacks() - set call back mocks - actual mocks of console view service are same as the callback mocks`() {
        consoleViewService.setCallbacks(
            goToFirstPageCallback,
            goToLastPageCallback,
            goToNextPageCallback,
            goToPreviousPageCallback,
            goToPageCallback
        )

        assertEquals(goToFirstPageCallback, consoleViewService.goToFirstPageCallback )
        assertEquals(goToLastPageCallback, consoleViewService.goToLastPageCallback )
        assertEquals(goToNextPageCallback, consoleViewService.goToNextPageCallback )
        assertEquals(goToPreviousPageCallback, consoleViewService.goToPreviousPageCallback )
        assertEquals(goToPageCallback, consoleViewService.goToPageCallback )
    }

    @Test
    fun `start() - first start of the service - invoke goToFirstPageCallback and print returned table page`() {
        val table = Table("H","S", listOf("A","B","C"))
        val tablePage = TablePage(table, 0, 1)
        whenever(goToFirstPageCallback.invoke()).thenReturn( tablePage)

        consoleViewService.setCallbacks(
            goToFirstPageCallback,
            goToLastPageCallback,
            goToNextPageCallback,
            goToPreviousPageCallback,
            goToPageCallback
        )

        consoleViewService.exit = true
        consoleViewService.start()

        verify(goToFirstPageCallback, times(1)).invoke()
        val expected = "H\r\n"+
                "S\r\n" +
                "A\r\n" +
                "B\r\n" +
                "C\r\n" +
                "Page 1 of 2\r\n" +
                "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit\r\n" +
                "INFO: CSV-Viewer stopped."
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `handleInput() - handle input F - invoke goToFirstPageCallback`() {
        val table = Table("H","S", listOf("A","B","C"))
        val tablePage = TablePage(table, 0, 1)
        whenever(goToFirstPageCallback.invoke()).thenReturn( tablePage)

        consoleViewService.setCallbacks(
            goToFirstPageCallback,
            goToLastPageCallback,
            goToNextPageCallback,
            goToPreviousPageCallback,
            goToPageCallback
        )

        consoleViewService.handleInput("F")

        verify(goToFirstPageCallback, times(1)).invoke()
    }

    @Test
    fun `handleInput() - handle input L - invoke goToLastPageCallback`() {
        val table = Table("H","S", listOf("A","B","C"))
        val tablePage = TablePage(table, 0, 1)
        whenever(goToLastPageCallback.invoke()).thenReturn(tablePage)

        consoleViewService.setCallbacks(
            goToFirstPageCallback,
            goToLastPageCallback,
            goToNextPageCallback,
            goToPreviousPageCallback,
            goToPageCallback
        )

        consoleViewService.handleInput("L")

        verify(goToLastPageCallback, times(1)).invoke()
    }

    @Test
    fun `handleInput() - handle input P - invoke goToPreviousPageCallback`() {
        val table = Table("H","S", listOf("A","B","C"))
        val tablePage = TablePage(table, 0, 1)
        whenever(goToPreviousPageCallback.invoke()).thenReturn(tablePage)

        consoleViewService.setCallbacks(
            goToFirstPageCallback,
            goToLastPageCallback,
            goToNextPageCallback,
            goToPreviousPageCallback,
            goToPageCallback
        )

        consoleViewService.handleInput("P")

        verify(goToPreviousPageCallback, times(1)).invoke()
    }

    @Test
    fun `handleInput() - handle input N - invoke goToNextPageCallback`() {
        val table = Table("H","S", listOf("A","B","C"))
        val tablePage = TablePage(table, 0, 1)
        whenever(goToNextPageCallback.invoke()).thenReturn(tablePage)

        consoleViewService.setCallbacks(
            goToFirstPageCallback,
            goToLastPageCallback,
            goToNextPageCallback,
            goToPreviousPageCallback,
            goToPageCallback
        )

        consoleViewService.handleInput("N")

        verify(goToNextPageCallback, times(1)).invoke()
    }

    @Test
    fun `handleInput() - handle input J and 1 - invoke goToPageCallback`() {
        val table = Table("H","S", listOf("A","B","C"))
        val tablePage = TablePage(table, 0, 1)
        whenever(goToPageCallback.invoke(any())).thenReturn(tablePage)

        consoleViewService.setCallbacks(
            goToFirstPageCallback,
            goToLastPageCallback,
            goToNextPageCallback,
            goToPreviousPageCallback,
            goToPageCallback
        )

        consoleViewService.handleInput("J")
        consoleViewService.handleInput("1")

        verify(goToPageCallback, times(1)).invoke(0)
    }

    @Test
    fun `handleInput() - handle input E - exit = true`() {
        consoleViewService.handleInput("E")

        assertTrue(consoleViewService.exit)
    }

    @Test
    fun `handleInput() - handle unsupported input X - print command not supported`() {
        consoleViewService.handleInput("X")

        val expected = "Command not supported: F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit"
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `printCommandNotSupported() - print command not supported - command not supported as console output`() {
        consoleViewService.printCommandNotSupported()

        val expected = "Command not supported: F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit"
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `printPageSize() - print page size of 2 - page size as console output`() {
        consoleViewService.printPageSize(2)

        val expected = "INFO: Using page size of 2."
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `printDefaultPageSize() - print default page size of 2 - default page size as console output`() {
        consoleViewService.printDefaultPageSize(2)

        val expected = "WARNING: Could not parse page size! Using default page size of 2."
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `printExitingCsvViewer() - print exiting csv viewer - exiting csv viewer as console output`() {
        consoleViewService.printExitingCsvViewer()

        val expected = "ERROR: No arguments specified. Exiting CSV-Viewer!"
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `executeAndRefreshView() - execute callback and refresh view with returned page - exiting csv viewer as console output`() {
        val table = Table("H","S", listOf("A","B","C"))
        val tablePage = TablePage(table, 0, 1)
        val callback = fun(): TablePage { return tablePage}

        consoleViewService.executeAndRefreshView(callback)

        val expected = "H\r\n"+
                "S\r\n" +
                "A\r\n" +
                "B\r\n" +
                "C\r\n" +
                "Page 1 of 2\r\n" +
                "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit"
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `refreshConsoleView() - example table page - table page console output and helper text`() {

        val table = Table("H","S", listOf("A","B","C"))
        val tablePage = TablePage(table, 0, 1)

        consoleViewService.refreshConsoleView(tablePage)

        val expected = "H\r\n"+
            "S\r\n" +
            "A\r\n" +
            "B\r\n" +
            "C\r\n" +
            "Page 1 of 2\r\n" +
            "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit"
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @AfterEach
    fun tearDown() {
        validateMockitoUsage();
        System.setOut(standardOut)
    }
}
