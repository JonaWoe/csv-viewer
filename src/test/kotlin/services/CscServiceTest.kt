package services

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class CscServiceTest {

    private lateinit var csvService: CsvService

    companion object {
        const val resourcePath = "src/test/resources"
    }

    @BeforeEach
    fun setUp() {
        csvService = CsvService()
    }

    @Test
    fun `readCsvFile() - read input csv file - lines as a list of strings`() {
        val csvFilename = "$resourcePath/input.csv"

        val actual = csvService.readCsvFile(csvFilename)

        val expected = listOf("A", "B", "C")
        assertEquals(expected, actual)
    }

}
