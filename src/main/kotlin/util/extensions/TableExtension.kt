package util.extensions

import models.Table

fun Table.build(): List<String> {
    return listOf(header, separator).plus(content)
}

fun Table.copy(newHeader: String? = null, newSeparator: String? = null , newContent: List<String>? = null ): Table {
    val header = newHeader ?: header
    val separator = newSeparator?: separator
    val content = newContent?: content
    return Table(header, separator, content)
}
