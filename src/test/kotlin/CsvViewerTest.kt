import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.MockitoAnnotations
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.PrintStream
import kotlin.test.assertEquals


class CsvViewerTest {


    private lateinit var csvViewer: CsvViewer

    private val standardOut = System.out
    private val outputStreamCaptor = ByteArrayOutputStream()


    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        System.setOut(PrintStream(outputStreamCaptor));
    }


    @Test
    fun `init{} - init csv viewer with correct args - output of first page`() {
        val args = arrayOf("src/test/resources/persons.csv", "2")
        csvViewer = CsvViewer(args)

        val expected = "INFO: CSV-Viewer started.\r\n" +
                "INFO: Using page size of 2.\r\n" +
                "No.|Name     |Age|City     |\r\n" +
                "---+---------+---+---------+\r\n" +
                "1. |Peter    |42 |New York |\r\n" +
                "2. |Paul     |57 |London   |\r\n" +
                "Page 1 of 4\r\n" +
                "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit\r\n" +
                "ERROR: Could not read console input exiting. Exiting CSV-Viewer!"
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `init{} - init csv viewer with correct args and input L - output of last page`() {
        val args = arrayOf("src/test/resources/persons.csv", "2")

        val input = "L"
        val inputStream: InputStream = ByteArrayInputStream(input.toByteArray())
        System.setIn(inputStream)

        csvViewer = CsvViewer(args)

        val expected = "INFO: CSV-Viewer started.\r\n" +
                "INFO: Using page size of 2.\r\n" +
                "No.|Name     |Age|City     |\r\n" +
                "---+---------+---+---------+\r\n" +
                "1. |Peter    |42 |New York |\r\n" +
                "2. |Paul     |57 |London   |\r\n" +
                "Page 1 of 4\r\n" +
                "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit\r\n" +
                "No.|Name     |Age|City     |\r\n" +
                "---+---------+---+---------+\r\n" +
                "7. |Nadia    |29 |Madrid   |\r\n" +
                "Page 4 of 4\r\n" +
                "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit\r\n" +
                "ERROR: Could not read console input exiting. Exiting CSV-Viewer!"
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `init{} - init csv viewer with correct args and input N - output of next page`() {
        val args = arrayOf("src/test/resources/persons.csv", "2")

        val input = "N"
        val inputStream: InputStream = ByteArrayInputStream(input.toByteArray())
        System.setIn(inputStream)

        csvViewer = CsvViewer(args)

        val expected = "INFO: CSV-Viewer started.\r\n" +
                "INFO: Using page size of 2.\r\n" +
                "No.|Name     |Age|City     |\r\n" +
                "---+---------+---+---------+\r\n" +
                "1. |Peter    |42 |New York |\r\n" +
                "2. |Paul     |57 |London   |\r\n" +
                "Page 1 of 4\r\n" +
                "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit\r\n" +
                "No.|Name     |Age|City     |\r\n" +
                "---+---------+---+---------+\r\n" +
                "3. |Mary     |35 |Munich   |\r\n" +
                "4. |Jaques   |66 |Paris    |\r\n" +
                "Page 2 of 4\r\n" +
                "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit\r\n" +
                "ERROR: Could not read console input exiting. Exiting CSV-Viewer!"
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `init{} - init csv viewer with correct args and input P - output of previous page`() {
        val args = arrayOf("src/test/resources/persons.csv", "2")

        val input = "P"
        val inputStream: InputStream = ByteArrayInputStream(input.toByteArray())
        System.setIn(inputStream)

        csvViewer = CsvViewer(args)

        val expected = "INFO: CSV-Viewer started.\r\n" +
                "INFO: Using page size of 2.\r\n" +
                "No.|Name     |Age|City     |\r\n" +
                "---+---------+---+---------+\r\n" +
                "1. |Peter    |42 |New York |\r\n" +
                "2. |Paul     |57 |London   |\r\n" +
                "Page 1 of 4\r\n" +
                "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit\r\n" +
                "No.|Name     |Age|City     |\r\n" +
                "---+---------+---+---------+\r\n" +
                "1. |Peter    |42 |New York |\r\n" +
                "2. |Paul     |57 |London   |\r\n" +
                "Page 1 of 4\r\n" +
                "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit\r\n" +
                "ERROR: Could not read console input exiting. Exiting CSV-Viewer!"
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @Test
    fun `init{} - init csv viewer with correct args and input J - output command info`() {
        val args = arrayOf("src/test/resources/persons.csv", "2")

        val input = "J"
        val inputStream: InputStream = ByteArrayInputStream(input.toByteArray())
        System.setIn(inputStream)

        csvViewer = CsvViewer(args)

        val expected = "INFO: CSV-Viewer started.\r\n" +
                "INFO: Using page size of 2.\r\n" +
                "No.|Name     |Age|City     |\r\n" +
                "---+---------+---+---------+\r\n" +
                "1. |Peter    |42 |New York |\r\n" +
                "2. |Paul     |57 |London   |\r\n" +
                "Page 1 of 4\r\n" +
                "F)irst page, P)revious page, N)ext page, L)ast page, J)ump to page, E)xit\r\n" +
                "INFO: Jump to page selected, insert page number:\r\n" +
                "ERROR: Could not read console input exiting. Exiting CSV-Viewer!"
        assertEquals(expected, outputStreamCaptor.toString().trim())
    }

    @AfterEach
    fun tearDown() {
        System.setOut(standardOut)
    }
}
