package services

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import util.exceptions.ArgsNotSpecifiedException
import view.ConsoleViewService
import kotlin.test.assertEquals

class CommandLineServiceTest {


    private lateinit var commandLineService: CommandLineService

    @Mock
    private lateinit var consoleViewService: ConsoleViewService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        val args = arrayOf("/test/path", "2")
        commandLineService = CommandLineService(args, consoleViewService)
    }

    @Test
    fun `parsePageSize() - parse page size from args without error - page size of 2 `() {
        val args = arrayOf("/test/path", "2")
        commandLineService = CommandLineService(args, consoleViewService)

        val actual = commandLineService.parsePageSize()

        assertEquals(2, actual)
    }

    @Test
    fun `parsePageSize() - parse page size from args with parsing error - page size of 15 `() {
        val args = arrayOf("/test/path", "A")
        commandLineService = CommandLineService(args, consoleViewService)

        val actual = commandLineService.parsePageSize()

        assertEquals(20, actual)
    }


    @Test
    fun `parseCsvFileName() - parse csv file name from args without parsing error - test path `() {
        val args = arrayOf("/test/path", "A")
        commandLineService = CommandLineService(args, consoleViewService)

        val actual = commandLineService.parseCsvFileName()

        assertEquals("/test/path", actual)
    }

    @Test
    fun `init{} - parse empty args - throw args not specified exception `() {
        val args = emptyArray<String>()

        assertThrows<ArgsNotSpecifiedException> { CommandLineService(args, consoleViewService) }
    }

}
