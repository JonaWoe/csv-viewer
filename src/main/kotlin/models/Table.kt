package models

data class Table(
    var header: String,
    var separator: String,
    var content: List<String>
)
