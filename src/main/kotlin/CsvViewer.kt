import interactors.TableInteractor
import services.CommandLineService
import services.CsvService
import services.PageService
import services.TableService
import view.ConsoleViewService

class CsvViewer(args: Array<String>) {
    init {
        val consoleViewService = ConsoleViewService()
        val commandLineService = CommandLineService(args, consoleViewService)
        TableInteractor(commandLineService, consoleViewService, TableService(), CsvService(), PageService()  )
        consoleViewService.start()
    }
}
