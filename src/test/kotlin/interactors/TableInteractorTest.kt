package interactors

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import models.Table
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import services.CommandLineService
import services.CsvService
import services.PageService
import services.TableService
import view.ConsoleViewService


class TableInteractorTest {

    @Mock
    private lateinit var commandLineService: CommandLineService
    @Mock
    private lateinit var consoleViewService: ConsoleViewService
    @Mock
    private lateinit var tableService: TableService
    @Mock
    private lateinit var csvService: CsvService
    @Mock
    private lateinit var pageService: PageService

    private val tableExample = Table("H|", "-+", listOf("A|","B|", "C|", "D|", "E|", "F|"))

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun `init{} - init services end execute setup methods - return value of fullTable and pageSize`() {

        whenever(commandLineService.parsePageSize()).thenReturn(2)
        whenever(commandLineService.parseCsvFileName()).thenReturn("src/main/resources/persons.csv")
        whenever(tableService.formatTable(any())).thenReturn(tableExample)


        TableInteractor(commandLineService, consoleViewService, tableService, csvService, pageService )

        verify(commandLineService, times(1)).parseCsvFileName()
        verify(csvService, times(1)).readCsvFile(any())
        verify(tableService, times(1)).formatTable(any())
        verify(commandLineService, times(1)).parsePageSize()
        verify(pageService, times(1)).calculateLastPageCounter(any(), any())
        verify(consoleViewService, times(1)).setCallbacks(any(), any(), any(),any(), any())
    }

    @Test
    fun `goToFirstPage() - navigate to first page of the table - table page with first page`() {

        whenever(commandLineService.parsePageSize()).thenReturn(2)
        whenever(commandLineService.parseCsvFileName()).thenReturn("src/main/resources/persons.csv")
        whenever(tableService.formatTable(any())).thenReturn(tableExample)


        val tableInteractor = TableInteractor(commandLineService, consoleViewService, tableService, csvService, pageService )

        tableInteractor.goToFirstPage()

        verify(pageService, times(1)).setFirstPageCounter()
        verify(pageService, times(1)).filterPageContent(any(), any())
    }

    @Test
    fun `goToLastPage() - navigate to last page of the table - table page with last page`() {
        whenever(commandLineService.parsePageSize()).thenReturn(2)
        whenever(commandLineService.parseCsvFileName()).thenReturn("src/main/resources/persons.csv")
        whenever(tableService.formatTable(any())).thenReturn(tableExample)


        val tableInteractor = TableInteractor(commandLineService, consoleViewService, tableService, csvService, pageService )

        tableInteractor.goToLastPage()

        verify(pageService, times(1)).setLastPageCounter()
        verify(pageService, times(1)).filterPageContent(any(), any())
    }

    @Test
    fun `goToPreviousPage() - navigate to previous page of the table - table page with previous page`() {
        whenever(commandLineService.parsePageSize()).thenReturn(2)
        whenever(commandLineService.parseCsvFileName()).thenReturn("src/main/resources/persons.csv")
        whenever(tableService.formatTable(any())).thenReturn(tableExample)


        val tableInteractor = TableInteractor(commandLineService, consoleViewService, tableService, csvService, pageService )

        tableInteractor.goToPreviousPage()

        verify(pageService, times(1)).setPreviousPageCounter()
        verify(pageService, times(1)).filterPageContent(any(), any())
    }

    @Test
    fun `goToNextPage() - navigate to next page of the table - table page with next page`() {
        whenever(commandLineService.parsePageSize()).thenReturn(2)
        whenever(commandLineService.parseCsvFileName()).thenReturn("src/main/resources/persons.csv")
        whenever(tableService.formatTable(any())).thenReturn(tableExample)

        val tableInteractor = TableInteractor(commandLineService, consoleViewService, tableService, csvService, pageService )

        tableInteractor.goToNextPage()

        verify(pageService, times(1)).setNextPageCounter()
        verify(pageService, times(1)).filterPageContent(any(), any())
    }

    @Test
    fun `goToPage() - navigate to specific page of the table - table page with specific page`() {
        whenever(commandLineService.parsePageSize()).thenReturn(2)
        whenever(commandLineService.parseCsvFileName()).thenReturn("src/main/resources/persons.csv")
        whenever(tableService.formatTable(any())).thenReturn(tableExample)

        val tableInteractor = TableInteractor(commandLineService, consoleViewService, tableService, csvService, pageService )

        tableInteractor.goToPage(2)

        verify(pageService, times(1)).setPageCounter(any())
        verify(pageService, times(1)).filterPageContent(any(), any())
    }
}
