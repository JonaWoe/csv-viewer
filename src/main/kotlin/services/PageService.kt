package services

import models.Table
import models.TablePage
import util.extensions.copy

class PageService {

    private var currentPageCounter = 0
    private var lastPageCounter = 0

    fun filterPageContent(fullTable: Table, pageSize: Int): TablePage {
        val pageStart = pageSize * (currentPageCounter)
        val pageEnd = pageSize * (currentPageCounter + 1)
        val newPageContent = fullTable.content.filterIndexed { index, _ -> index in (pageStart) until pageEnd }
        return TablePage(fullTable.copy(newContent = newPageContent), currentPageCounter, lastPageCounter)
    }

    fun setPreviousPageCounter() {
        currentPageCounter = if (currentPageCounter-1>0) currentPageCounter-1 else 0
    }

    fun setNextPageCounter() {
        currentPageCounter = if (currentPageCounter+1<=lastPageCounter) currentPageCounter+1 else lastPageCounter
    }

    fun setLastPageCounter() {
        currentPageCounter = lastPageCounter
    }

    fun setFirstPageCounter() {
        currentPageCounter = 0
    }

    fun setPageCounter(pageCounter: Int) {
        currentPageCounter = if (pageCounter in 0..lastPageCounter) pageCounter else currentPageCounter
    }

    fun calculateLastPageCounter(contentSize: Int, pageSize: Int) {
        lastPageCounter = Math.ceilDiv(contentSize, pageSize)-1
    }
}
