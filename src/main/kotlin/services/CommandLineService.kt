package services

import util.exceptions.ArgsNotSpecifiedException
import view.ConsoleViewService

class CommandLineService(private val args: Array<String>, private val consoleViewService: ConsoleViewService) {

    companion object {
        private const val DEFAULT_PAGE_SIZE = 20
    }

    init {
        if (args.isEmpty()) {
            consoleViewService.printExitingCsvViewer()
            throw ArgsNotSpecifiedException("ERROR: No arguments specified. Exiting CSV-Viewer!")
        }
    }

    fun parseCsvFileName(): String {
        return args.first()
    }

    fun parsePageSize(): Int {
        return try {
            val unparsedPageSize = args.last()
            val pageSize = Integer.parseInt(unparsedPageSize)
            consoleViewService.printPageSize(pageSize)
            pageSize
        } catch (exception: Exception) {
            consoleViewService.printDefaultPageSize(DEFAULT_PAGE_SIZE)
            DEFAULT_PAGE_SIZE
        }
    }
}
